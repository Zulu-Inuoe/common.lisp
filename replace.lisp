(in-package #:cl-user)

(defun :replace (sequence1 sequence2 &optional replacement)
  (loop :with result-type := (etypecase sequence1
                                (simple-string 'simple-string)
                                (string 'string)
                                (vector 'vector)
                                (list 'list))
        :with result := sequence1
        :for start := 0 :then (+ idx (length replacement))
        :for idx := (search sequence2 result :start2 start)
        :while idx
        :do (setf result (concatenate result-type (subseq result 0 idx) replacement (subseq result (+ idx (length sequence2)))))
        :finally (return result)))
