(in-package #:cl-user)

(:import+ #:cl-ansi-text #:fiveam)

(defmethod fiveam:explain ((exp fiveam::detailed-text-explainer) (results list)
                           &optional (stream fiveam:*test-dribble*) (recursive-depth 0))
  (multiple-value-bind (num-checks passed num-passed passed%
                                   skipped num-skipped skipped%
                                   failed num-failed failed%
                                   unknown num-unknown unknown%)
      (fiveam::partition-results results)
    (declare (ignore passed))
    (flet ((output (&rest format-args)
             (format stream "~&~vT" recursive-depth)
             (apply #'format stream format-args)))

      (when (zerop num-checks)
        (output "Didn't run anything...huh?")
        (return-from fiveam:explain nil))
      (terpri stream)
      (terpri stream)
      (output "Did ~D check~P.~%" num-checks num-checks)
      (output (cl-ansi-text:green "   Pass: ~D (~2D%)") num-passed passed%)
      (output (cl-ansi-text:red "   Fail: ~D (~2D%)") num-failed failed%)
      (output (cl-ansi-text:yellow "   Skip: ~D (~2D%)") num-skipped skipped%)
      (when unknown
        (output "   UNKNOWN RESULTS: ~D (~2D)~%" num-unknown unknown%))
      (terpri stream)
      (when failed
        (terpri stream)
        (output "Failure Details:~%")
        (dolist (f (reverse failed))
          (output "--------------------------------~%")
          (output "~A~@[ in ~A~]~@{~@[[~A]~]~}:~%"
                  (cl-ansi-text:red (string (fiveam::name (fiveam::test-case f))))
                  (let ((suite (fiveam::test-suite (fiveam::test-case f))))
                    (when suite (fiveam::name suite)))
                  (let ((description (fiveam::description (fiveam::test-case f))))
                    (if (/= 0 (length description))
                        description
                        nil)))
          (output "     ~A~%" (fiveam::reason f))
          (when (fiveam::for-all-test-failed-p f)
            (output "Results collected with failure data:~%")
            (fiveam:explain exp (slot-value f 'result-list)
                            stream (+ 4 recursive-depth)))
          (when (and fiveam:*verbose-failures* (fiveam::test-expr f))
            (output "    ~S~%" (fiveam::test-expr f)))
          (output "--------------------------------~%"))
        (terpri stream))
      (when skipped
        (terpri stream)
        (output "Skip Details:~%")
        (dolist (f skipped)
          (output "~A ~@{[~A]~}: ~%"
                  (cl-ansi-text:yellow (string (fiveam::name (fiveam::test-case f))))
                  (fiveam::description (fiveam::test-case f)))
          (output "    ~A~%" (fiveam::reason f)))
        (terpri stream)))))

(in-package #:fiveam)

(handler-bind ((style-warning #'muffle-warning))
  (defun fiveam::add-result (result-type &rest make-instance-args)
    "Create a TEST-RESULT object of type RESULT-TYPE passing it the
  initialize args MAKE-INSTANCE-ARGS and add the resulting
  object to the list of test results."
    (fiveam::with-run-state (fiveam::result-list fiveam::current-test)
      (when (boundp 'current-test)
        (let ((result (apply #'make-instance result-type
                             (append make-instance-args (list :test-case current-test)))))
          (etypecase result
            (fiveam::test-passed  (format fiveam::*test-dribble* (cl-ansi-text:green "✔")))
            (fiveam::unexpected-test-failure (format fiveam::*test-dribble* (cl-ansi-text:red "🔥")))
            (fiveam::test-failure (format fiveam::*test-dribble* (cl-ansi-text:red "❌")))
            (fiveam::test-skipped (format fiveam::*test-dribble* "⏩")))
          (push result fiveam::result-list))))))
