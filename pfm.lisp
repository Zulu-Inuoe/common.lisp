(:include "id")
(:include "guid")

(defun :make-client (&key
                       (name "<name>")
                       (company "<company>")
                       (id (:make-id))
                       (secret (subseq (string-downcase (:make-guid)) 1 37))
                       (data-source "flt")
                       (product-ids ())
                     &aux
                       (product-ids (if (not (listp product-ids))
                                        (list product-ids)
                                        product-ids)))
  (format nil
          "  {
    \"Id\": \"~A\",
    \"Name\": \"~A\",
    \"ClientId\": \"~A\",
    \"ClientSecret\": \"~A\",
    \"Roles\": [ \"Client\", \"~A\" ],
    \"Scopes\": [ \"template:read\" ],
    \"PreCache\": true,
    \"OfflineUpdate\": true,
    \"DataSource\": \"~A\",
    \"MessageTypes\": [ ],
    \"MessageCodes\": [ ],
    \"ProductIds\": [~{ ~A~^,~} ],
    \"Profile\": {
      \"Company\": \"~A\",
      \"Division\": \"~A\"
    }
  }"
          id
          name
          id
          secret
          company
          data-source
          product-ids
          company
          name))
