(in-package #:cl-user)

(defun :string-natural-cmp (s1 s2)
  (cond
    ((zerop (length s1))
     (if (zerop (length s2)) 0 -1))
    ((zerop (length s2))
     (if (zerop (length s1)) 0 1))
    (t
     (labels((char-cmp (c1 c2)
               (cond
                 ((char-lessp c1 c2) -1)
                 ((char-greaterp c1 c2) 1)
                 (t 0)))
             (scan-num-end (s start)
               (let ((len (length s))
                     (nz-start start)
                     (end start)
                     (count-zeroes t))
                 (loop
                   (let ((c (char s end)))
                     (cond
                       ((not (digit-char-p c))
                        (return (values end nz-start)))
                       ((and count-zeroes (char= c #\0))
                        (incf nz-start))
                       (t
                        (setf count-zeroes nil)))
                     (incf end)
                     (when (>= end len)
                       (return (values end nz-start)))))))
             (num-cmp (s1 i1 s2 i2)
               (multiple-value-bind (end1 nz-start1) (scan-num-end s1 i1)
                 (multiple-value-bind (end2 nz-start2) (scan-num-end s2 i2)
                   (let ((start1 i1)
                         (i1 (1- end1))
                         (start2 i2)
                         (i2 (1- end2))
                         (nz-len1 (- end1 nz-start1))
                         (nz-len2 (- end2 nz-start2)))
                     (cond
                       ((< nz-len1 nz-len2) (values -1 i1 i2))
                       ((> nz-len1 nz-len2) (values  1 i1 i2))
                       (t
                        (loop :for j1 :from nz-start1 :upto i1
                              :for j2 :from nz-start2
                              :for r := (char-cmp (char s1 j1) (char s2 j2))
                              :unless (zerop r)
                                :return (values r j1 j2)
                              :finally
                                 (return
                                   (let ((l1 (- end1 start1))
                                         (l2 (- end2 start2)))
                                     (cond
                                       ((= l1 l2) (values 0  i1 i2))
                                       ((> l1 l2) (values -1 i1 i2))
                                       (t         (values 1  i1 i2)))))))))))))
       (let ((sp1 (alphanumericp (char s1 0)))
             (sp2 (alphanumericp (char s2 0))))
         (cond
           ((and sp1 (not sp2)) 1)
           ((and (not sp1) sp2) -1)
           (t
            (let ((i1 0)
                  (i2 0)
                  (r 0)
                  (l1 (length s1))
                  (l2 (length s2)))
              (loop
                (let* ((c1 (char s1 i1))
                       (c2 (char s2 i2))
                       (d1 (digit-char-p c1))
                       (d2 (digit-char-p c2)))
                  (cond
                    ((and (not d1) (not d2))
                     (let ((l1 (and (alpha-char-p c1) t))
                           (l2 (and (alpha-char-p c2) t)))
                       (cond
                         ((eq l1 l2)
                          (setf r (char-cmp c1 c2))
                          (unless (zerop r)
                            (return r)))
                         ((and (not l1) l2) (return -1))
                         ((and l1 (not l2)) (return 1)))))
                    ((and d1 d2)
                     (setf (values r i1 i2) (num-cmp s1 i1 s2 i2))
                     (unless (zerop r)
                       (return r)))
                    (d1
                     (return -1))
                    (d2
                     (return 1)))

                  (incf i1)
                  (incf i2)
                  (cond
                    ((and (>= i1 l1) (>= i2 l2))
                     (return 0))
                    ((>= i1 l1)
                     (return -1))
                    ((>= i2 l2)
                     (return 1)))))))))))))

(defun :string-natural< (s1 s2)
  (and (minusp (:string-natural-cmp s1 s2)) t))

(defun :string-natural<= (s1 s2)
  (not (plusp (:string-natural-cmp s1 s2))))

(defun :string-natural= (s1 s2)
  (zerop (:string-natural-cmp s1 s2)))

(defun :string-natural>= (s1 s2)
  (not (minusp (:string-natural-cmp s1 s2))))

(defun :string-natural> (s1 s2)
  (and (plusp (:string-natural-cmp s1 s2)) t))
