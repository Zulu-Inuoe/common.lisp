(in-package #:cl-user)

;; seed random
(setf *random-state* (make-random-state t))

(:include "quicklisp")
(:include "quicklisp+")

(:import+ #:named-readtables)
(:import+ #:closer-mop)

(use-package '#:named-readtables)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun %kvp->table-forms (kvp type)
    (let ((table-sym (gensym (format nil "~A-VALUE" type))))
      `(let ((,table-sym ,(ecase type
                            (:hash `(make-hash-table :test 'equal))
                            ((:alist :plist) `(list)))))
         ,@(loop
             :with setter-generator := (ecase type
                                         (:hash (lambda (k v) `(setf (gethash ,k ,table-sym) ,v)))
                                         (:alist (lambda (k v) `(setf (alexandria:assoc-value ,table-sym ,k) ,v)))
                                         (:plist (lambda (k v) `(setf (getf ,table-sym ,k) ,v))))
             :with cell := kvp
             :with key-sym := (gensym "KEY")
             :with value-sym := (gensym "VALUE")
             :collect
             (labels ((merge-hash (table-form)
                        `(maphash (lambda (,key-sym ,value-sym)
                                    ,(funcall setter-generator key-sym value-sym))
                                  ,table-form))
                      (merge-alist (alist-form)
                        `(loop
                           :for (,key-sym . ,value-sym) :in ,alist-form
                           :do ,(funcall setter-generator key-sym value-sym)))
                      (merge-plist (plist-form)
                        `(loop
                           :for (,key-sym ,value-sym) :on ,plist-form :by #'cddr
                           :do ,(funcall setter-generator key-sym value-sym)))
                      (merge-vector (vector-form)
                        `(loop
                           :for ,key-sym :from 0
                           :for ,value-sym :across ,vector-form
                           :do ,(funcall setter-generator key-sym value-sym)))
                      (merge-list (list-form)
                        `(loop
                           :for ,key-sym :from 0
                           :for ,value-sym :in ,list-form
                           :do ,(funcall setter-generator key-sym value-sym)))
                      (merge-sequence (sequence-form)
                        `(map nil (let ((,key-sym 0))
                                    (lambda (,value-sym)
                                      ,(funcall setter-generator key-sym value-sym)
                                      (incf ,key-sym)))
                              ,sequence-form))
                      (merge-on (type form)
                        (ecase type
                          ((:# :h :hash) (merge-hash form))
                          ((:a :alist) (merge-alist form))
                          ((:p :plist) (merge-plist form))
                          ((:v :vector) (merge-vector form))
                          ((:l :list) (merge-list form))
                          ((:s :sequence) (merge-sequence form)))))
               (cond
                 ((and (vectorp (car cell)) (not (stringp (car cell))))
                  (prog1 (merge-on type (aref (car cell) 0))
                    (setf cell (cdr cell))))
                 ((and (consp (car cell)) (eq (caar cell) :...))
                  (prog1 (merge-on type (second (car cell)))
                    (setf cell (cdr cell))))
                 ((consp (car cell))
                  (prog1 (destructuring-bind (type form) (car cell)
                           (merge-on type form))
                    (setf cell (cdr cell))))
                 ((cdr cell)
                  (destructuring-bind (k v &rest next) cell
                    (setf cell next)
                    (funcall setter-generator `(quote ,k) v)))
                 (t
                  (error "When constructing ~A: Bad number of arguments in ~S" type kvp))))
             :when (null cell)
               :do (loop-finish))
         ,table-sym))))

(defmacro :# (&rest kvp)
  "Pseudo-syntax for literla hash tables"
  (if kvp
      `(the hash-table ,(%kvp->table-forms kvp :hash))
      `(make-hash-table :test 'equal)))

(defmacro :h (&rest kvp)
  "Pseudo-syntax for literla hash tables"
  `(:# ,@kvp))

(defmacro :hash (&rest kvp)
  "Pseudo-syntax for literla hash tables"
  `(:# ,@kvp))

(macrolet ((hash-expander (symbol)
             `(define-setf-expander ,symbol (table &rest keys)
                (let ((table-sym (gensym "TABLE"))
                      (val-syms (mapcar (lambda (k) (declare (ignore k)) (gensym "VALUE")) keys)))
                  (values `(,table-sym)
                          `(,table)
                          `(,@val-syms)
                          `(setf (values ,@(mapcar (lambda (k) `(gethash ',k ,table-sym)) keys))
                                 (values ,@val-syms))
                          `(values ,@(mapcar (lambda (k) `(gethash ,k ',table-sym)) keys)))))))
  (hash-expander :#)
  (hash-expander :h)
  (hash-expander :hash))

(defun :@ (obj &rest keys)
  "Generalized value access function
  eg.
    (:@ (:v (:# 'x 'hello)) 0 'x 0)
    ; => #\H
    ; => T
"
  (labels ((:key-equal (k1 k2)
            (cond
              ((and (numberp k1) (numberp k2)) (= k1 k2))
              ((and (symbolp k1) (symbolp k2)) (eq k1 k2))
              ((and (or (symbolp k1) (stringp k1))
                    (or (symbolp k2) (stringp k2)))
                (string= k1 k2))))
           (:@ (obj key)
             (typecase obj
               (hash-table (gethash key obj))
               ((or vector symbol)
                 (let ((v (if (symbolp obj) (string obj) obj)))
                   (if (and (integerp key) (< -1 key (length v)))
                     (values (aref v key) t)
                     (values nil nil))))
               (list
                (let* ((alist (and (every #'consp obj)
                                   (loop :for (k . v) :in obj
                                         :always (or (numberp k) (symbolp k) (stringp k)))))
                       (plist (and (null alist)
                                   (evenp (length obj))
                                   (loop :for k :in obj :by #'cddr
                                         :always (or (numberp k) (symbolp k) (stringp k))))))
                  (cond
                    (alist
                      (let ((cell (assoc key obj :test #':key-equal)))
                        (values (car cell) (and cell t))))
                    (plist (loop :for (k v . rest) :on obj :by #'cddr
                                 :when (equal k key) :return (values v t)
                                 :finally (return (values nil nil))))
                    (t
                      (if (integerp key)
                        (let ((cell (nthcdr key obj)))
                          (values (car cell) (and cell t)))
                        (values nil nil))))))
               (t
                 (if (and (symbolp key) (slot-exists-p obj key))
                   (let ((boundp (slot-boundp obj key)))
                     (values (and boundp (slot-value obj key)) boundp))
                   (values nil nil))))))
    (loop :for o := obj :then value
          :for key :in keys
          :for (value foundp) := (multiple-value-list (:@ o key))
          :unless foundp :return (values nil nil)
          :finally (return (values o t)))))

(defmacro :a (&rest kvp)
  (if kvp
      `(nreverse ,(%kvp->table-forms kvp :alist))
      `(list)))

(defmacro :alist (&rest kvp)
  "Pseudo-syntax for literal alists"
  `(:a ,@kvp))

(defmacro :p (&rest kvp)
  "Pseudo-syntax for literal plists"
  (if kvp
      (%kvp->table-forms kvp :plist)
      `(list)))

(defmacro :plist (&rest kvp)
  "Pseudo-syntax for literal plists"
  `(:p ,@kvp))

(defmacro :v (&rest values)
  (if values
      (let ((vector-sym (gensym "VECTOR"))
            (length-sym (gensym "LENGTH")))
        `(let ((,vector-sym ())
               (,length-sym 0))
           ,@(loop
               :for v :in values
               :collect
               (cond
                 ((and (vectorp v) (not (stringp v)))
                  `(map nil (lambda (elt)
                              (push elt ,vector-sym)
                              (incf ,length-sym))
                        ,(aref v 0)))
                 (t
                  `(progn
                     (push ,v ,vector-sym)
                     (incf ,length-sym)))))
           (make-array ,length-sym :initial-contents (nreverse ,vector-sym))))
      `(vector)))

(defmacro :vector (&rest values)
  `(:v ,@values))

(defun :alist-remove (alist keys &key key test)
  "Remove the given `keys' from `alist'"
  (remove-if (lambda (cell) (member (car cell) keys :key key :test test))
             alist))

(declaim (inline :uint->int))
(defun :uint->int (size value)
  "unsigned integer -> signed integer conversion"
  (declare (type unsigned-byte size value))
  (logior value (- (mask-field (byte 1 (1- size)) value))))

(declaim (inline :uint->i8))
(defun :uint->i8 (value)
  (declare (type (unsigned-byte 8) value))
  (the (signed-byte 8) (:uint->int 8 value)))

(declaim (inline :uint->i16))
(defun :uint->i16 (value)
  (declare (type (unsigned-byte 16) value))
  (the (signed-byte 16) (:uint->int 16 value)))

(declaim (inline :uint->i32))
(defun :uint->i32 (value)
  (declare (type (unsigned-byte 32) value))
  (the (signed-byte 32) (:uint->int 32 value)))

(declaim (inline :uint->i64))
(defun :uint->i64 (value)
  (declare (type (unsigned-byte 64) value))
  (the (signed-byte 64) (:uint->int 64 value)))

(declaim (inline :int->uint))
(defun :int->uint (size value)
  "unsigned integer -> signed integer conversion"
  (declare (type unsigned-byte size)
           (type signed-byte value))
  (ldb (byte size 0) value))

(declaim (inline :int->u8))
(defun :int->u8 (value)
  (declare (type (signed-byte 8) value))
  (the (unsigned-byte 8) (:int->uint 8 value)))

(declaim (inline :int->u16))
(defun :int->u16 (value)
  (declare (type (signed-byte 16) value))
  (the (unsigned-byte 16) (:int->uint 16 value)))

(declaim (inline :int->u32))
(defun :int->u32 (value)
  (declare (type (signed-byte 32) value))
  (the (unsigned-byte 32) (:int->uint 32 value)))

(declaim (inline :int->u64))
(defun :int->u64 (value)
  (declare (type (signed-byte 64) value))
  (the (unsigned-byte 64) (:int->uint 64 value)))

(defun :falsy (value)
  "Return true if `value' is a falsy JSON value."
  (etypecase value
    ((or null (integer 0 0) (eql null))  t)
    (string (zerop (length value)))
    (double-float (zerop value))
    ((or hash-table simple-vector) nil)))

(defun :truthy (value)
  "Return true if `value' is a truthy JSON value."
  (not (:falsy value)))

#+sbcl (require '#:sb-cltl2)

(defun :missing-docstrings (package)
  (let ((package (if (packagep package) package (find-package package)))
        (results ()))
    (do-external-symbols (s package)
      (let ((class (find-class s nil))
            (functionp (fboundp s))
            (variablep (or (boundp s) #+sbcl (and (sb-cltl2:variable-information s) t)))
            (typep (ignore-errors (typep nil s) t))
            (entry ()))
        (when (and class (not (documentation class t)))
          (push 'class entry))
        (when (and functionp (not (documentation s 'function)))
          (push 'function entry))
        (when (and variablep (not (documentation s 'variable)))
          (push 'variable entry))
        (when (and typep (not class) (not (documentation s 'type)))
          (push 'type entry))

        (when entry
          (push (cons s entry) results))))
    results))

(:include "alert")
(:include "apropos+")
(:include "apropos-value")
(:include "arrows")
(:include "fiveam")
(:include "id")
(:include "guid")
(:include "make-project")
(:include "uiop+")
(:include "user")
(:include "package+")
(:include "pfm")
(:include "place")
(:include "print-hash-table")
(:include "replace")
(:include "seq")
(:include "string-natural-cmp")

(defun :de-bomify (thing)
  (etypecase thing
    (pathname
      (let ((octets (alexandria:read-file-into-byte-vector thing)))
        (when (and (>= (length octets) 3)
                   (= (aref octets 0) #xEF)
                   (= (aref octets 1) #xBB)
                   (= (aref octets 2) #xBF))
          (alexandria:write-byte-vector-into-file (subseq octets 3) thing :if-exists :supersede)
          t)))))
          
(defun :de-bomify-logging (paths)
  (loop :for path :in paths
        :when (:de-bomify path)
        :do (format t "Fixed '~A'~%" path)))

#+sbcl
(require "SB-ACLREPL")

#++
(defmethod print-object ((object standard-object) stream)
  (print-unreadable-object (object stream :type t)
    (pprint-logical-block (stream nil)
      (let ((class (class-of object))
            (first t))
        (closer-mop:ensure-finalized class)
        (dolist (slot (closer-mop:class-slots class))
          (when (closer-mop:slot-boundp-using-class class object slot)
            (if first
                (setf first nil)
                (pprint-newline :mandatory stream))
            (let ((name (closer-mop:slot-definition-name slot))
                  (value (closer-mop:slot-value-using-class class object slot)))
              (format stream "~A: ~A" name value))))))))

(setf *print-circle* t)

(defreadtable zulu
  (:merge :standard)
  (:macro-char #\{ #':hash-table-reader)
  (:macro-char #\} (lambda (stream char)
                     (declare (ignore stream char))
                     (error "unmatched close bracket"))))

(in-readtable zulu)
