(in-package #:cl-user)

#+sbcl
(progn
  (sb-ext:restrict-compiler-policy 'debug 3 3)
  (sb-ext:restrict-compiler-policy 'safety 3 3))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun :compile-file-temp (file 
                               &aux
                                 (this-dir
                                   #.(let ((pathname (or *load-truename*
                                                         *compile-file-truename*
                                                         *default-pathname-defaults*)))
                                       (make-pathname :host (pathname-host pathname)
                                                      :device (pathname-device pathname)
                                                      :directory (pathname-directory pathname))))
                                 (fasls-dir (merge-pathnames (make-pathname :directory '(:relative "bin" "fasls")) this-dir)))
    (let* ((file (merge-pathnames file (make-pathname :type "lisp")))
           (file (probe-file file))
           (pathname-directory (list* :relative (concatenate 'string (lisp-implementation-type) "-" (lisp-implementation-version)) #+win32 (pathname-device file) (cdr (pathname-directory file))))
           (compile-file (merge-pathnames (make-pathname :name (pathname-name file) :type "fasl" :directory pathname-directory) fasls-dir)))
      (or (probe-file compile-file)
          (progn (ensure-directories-exist compile-file)
                 (compile-file file :output-file compile-file))))))

#+sbcl
(eval-when (:compile-toplevel :load-toplevel :execute)  
  (defun :asdf-module-provider-fn (name)
    (when (string-equal name "asdf")
      (load (:compile-file-temp (merge-pathnames (make-pathname :name "asdf")
                                                 #.(or *load-truename*
                                                       *compile-file-truename*
                                                       *default-pathname-defaults*))))))
  (pushnew :asdf-module-provider-fn sb-ext:*module-provider-functions*))

#+sbcl
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun :uiop-module-provider-fn (name)
    (when (string-equal name "uiop")
      (load (:compile-file-temp (merge-pathnames (make-pathname :name "uiop")
                                                 #.(or *load-truename*
                                                       *compile-file-truename*
                                                       *default-pathname-defaults*))))))
  (pushnew :uiop-module-provider-fn sb-ext:*module-provider-functions*))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (require "asdf")
  (require "uiop"))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (member "--no-quicklisp" (uiop:command-line-arguments) :test #'string=)
    (load (merge-pathnames (make-pathname :name "quicklisp" :type "lisp") #.(or *compile-file-truename* *load-truename*)))))

;; Load up flexi-streams in case we're using Sly
#+lispworks
(ql:quickload '#:flexi-streams :silent t)

#-lispworks
(progn
  (defmacro :eval-always (&body body)
    `(eval-when (:compile-toplevel :load-toplevel :execute)
       ,@body))

  (defun :arg (arg)
    (cadr (member arg (uiop:command-line-arguments) :test #'string=)))

  (defun :argp (arg)
    (and (member arg (uiop:command-line-arguments) :test #'string=)
         t))

  (defmacro :unless-arg (arg &body body)
    `(:eval-always
       (unless (:argp ,arg)
         ,@body)))

  (defmacro :when-arg (arg &body body)
    (destructuring-bind (arg-name &optional arg-value-sym)
        (uiop:ensure-list arg)
      (let ((tmp-sym (gensym "TMP")))
        `(:eval-always
           (let ((,tmp-sym (:arg ,arg-name)))
             (when ,tmp-sym
               ,@(if arg-value-sym
                     `((let ((,arg-value-sym ,tmp-sym))
                         ,@body))
                     body)))))))

  (defun :arg= (arg value)
    (let ((arg-val (:arg arg)))
      (and arg-val (string= arg-val value))))

  (defmacro :include (file)
    (let ((file (macroexpand file)))
      (assert (constantp file) (file))
      (setf file (eval file))
      (check-type file (or string pathname))
      `(progn
         (eval-when (:compile-toplevel)
           (compile-file (merge-pathnames ',file (or *compile-file-truename* *default-pathname-defaults*))))
         (eval-when (:load-toplevel :execute)
           (load (merge-pathnames ',file (or *load-truename* *default-pathname-defaults*)))))))

  (:unless-arg "--no-quicklisp"
    (:include "quicklisp"))

  (defmacro :import+ (&rest systems)
    `(eval-when (:compile-toplevel :load-toplevel :execute)
       ,@(mapcar (lambda (system-designator)
                   (multiple-value-bind (system-designator nickname)
                       (let ((sys (uiop:ensure-list system-designator)))
                         (cond
                           ((null (cdr sys)) (values (first sys) nil))
                           ((null (cddr sys)) (values (first sys) (second sys)))
                           (t (error "Bad :import+ designator: '~A'" sys))))
                     `(handler-bind ((asdf:bad-system-name #'muffle-warning))
                        (let ((system (asdf:find-system ',system-designator nil)))
                          (unless (and system (asdf:component-loaded-p system))
                            (let* ((ql (find-package "QUICKLISP"))
                                   (quickload (and ql (find-symbol "QUICKLOAD" ql))))
                              (if quickload
                                  (funcall quickload '(,system-designator) :verbose nil :silent t)
                                  (asdf:load-system ',system-designator)))))
                        ,@(when nickname
                            `((sb-ext:add-package-local-nickname ',nickname (find-package ',system-designator)))))))
                 systems)))

  (:unless-arg "--no-utils"
    (:include "utils"))

  (:when-arg ("--compile" thing)
    (unless thing
      (format *error-output* "error: Missing argument to --compile")
      (uiop:quit 1))
    (let* ((truename (probe-file thing))
           (out-name (or (:arg "-o")
                         (:arg "--output")
                         (unless (and truename (string= (pathname-type truename) "asd"))
                           (merge-pathnames (make-pathname :type "fasl")
                                            truename))))
           (out-type
             (cond
               ((null out-name) nil)
               ((string= (pathname-type out-name) "fasl") :fasl)
               ((string= (pathname-type out-name) "exe") :exe)))
           (entry-point (string-upcase (or (:arg "-e")
                                           (:arg "--entry")
                                           "CL-USER::MAIN"))))
      (cond
        ((null truename)
         (format *error-output* "error: Cannot find file '~A'" thing)
         (uiop:quit 1))
        ((uiop:directory-pathname-p truename)
         (format *error-output* "error: Pathname is a directory: '~A'" truename)
         (uiop:quit 1))
        ((and (pathname-type truename) (string-equal (pathname-type truename) "asd"))
         (cond
           ((eq out-type nil)
            (let ((zerop nil))
              (unwind-protect
                   (progn
                     (let ((*debugger-hook*
                             (lambda (c h)
                               (declare (ignore h))
                               (format *error-output* "error: ~A" c)
                               (uiop:quit 1))))
                       (asdf:load-asd truename)
                       (asdf:load-system (pathname-name truename) :verbose (:argp "--verbose") :force (:argp "--force")))
                     (setf zerop t))
                (uiop:quit (if zerop 0 1)))))
           (t
            (format *error-output* "error: unsupported out-type '~A'" out-type)
            (uiop:quit 1))))
        (t
         (cond
           ((eq out-type :fasl)
            (let ((zerop nil))
              (unwind-protect
                  (handler-case
                      (let ((*compile-verbose*  (:argp "--verbose")))
                        (setf zerop (not (nth-value 2 (compile-file truename :output-file out-name)))))
                    (error (e)
                      (format *error-output* "error: ~A" e)
                      (uiop:quit 1)))
                (uiop:quit (if zerop 0 1)))))
           ((eq out-type :exe)
            (let ((zerop nil)
                  (file nil))
              (unwind-protect
                   (handler-case
                       (let ((*compile-verbose*  (:argp "--verbose")))
                         (multiple-value-bind (out-file warnings-p failure-p) (compile-file truename)
                           (declare (ignore warnings-p))
                           (when (not failure-p)
                             (load out-file :verbose (:argp "--verbose"))
                             #+sbcl
                             (sb-ext:save-lisp-and-die out-name :toplevel (read-from-string entry-point) :executable t)
                             #-sbcl
                             (format *error-output* "error: executable dumping unsupported")
                             (uiop:quit 1))
                           (setf zerop (not failure-p)
                                 file out-file)))
                     (error (e)
                       (format *error-output* "error: ~A" e)
                       (uiop:quit 1)))
                (uiop:quit (if zerop 0 1)))))
           (t
            (format *error-output* "error: unsupported out-type: '~A'" out-type)
            (uiop:quit 1))))))))
