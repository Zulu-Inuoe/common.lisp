(in-package #:cl-user)

(:import+ #:com.inuoe.seq
          #:com.inuoe.seqio
          #:com.inuoe.doseq
          #:com.inuoe.seq.generators)

(uiop:define-package #:com.inuoe.seq-meta
  (:use-reexport #:com.inuoe.seq)
  (:use-reexport #:com.inuoe.seqio)
  (:use-reexport #:com.inuoe.doseq)
  (:use-reexport #:com.inuoe.seq.generators))

(add-package-local-nickname '#:seq '#:com.inuoe.seq-meta)
